/**
 * Run this command in the target path
 * This creates the ESLint errors in a nice JSON file.
 */

// `./node_modules/.bin/eslint -f=json --no-eslintrc --plugin=import --rule=import/no-unresolved:error -o=eslint-errors.json src/**/*.js
// -o=eslint-errors.json src/**/*.js`

const fs = require('fs');
const glob = require('glob');
const path = require('path');
const semver = require('semver');
const async = require('async');

const TARGET_PATH = '../npm-components/';
const MAIN_PKG = '/Users/ed/clients/clearscore/npm-components/package.json';
const errorsFile = require(`${TARGET_PATH}eslint-errors.json`);
const yarnPath = `${TARGET_PATH}yarn.lock`;

const atomicLib = [
  'atom',
  'molecule',
  'organism',
  'page'
];

const otherLib = [
  'scaffolding',
  'redux',
  'helpers',
  'connect',
  'lambda',
  'config',
];
const clearscorePrefixes = [...atomicLib, ...otherLib].map(i => `@${i}`);
const moduleIsClearScored = (name) => clearscorePrefixes.find((n) => name.startsWith(n));

function getYarnVersion() {
  const yarnModuleRegex = /([\w|\-|\.]+)\@[\D]*?([0-9]+\.[0-9]+\.[0-9]+)[\,|:]/gm;

  return new Promise((resolve, reject) => {
    fs.readFile(yarnPath, 'utf8', (err, data) => {
      if (err) throw err;
      const items = {};
      const matches_array = data.match(yarnModuleRegex);
      matches_array.forEach((str) => {
        const parts = str.split('@');
        const name = parts[0];
        const version = parts[1].replace(/[^0-9|\.]/g, '');
        if ((items[name] && semver.gt(version, items[name])) || !items[name]) {
          items[name] = version;
        }
      });
      resolve(items);
    });
  });
}

function getPackageVersions() {
  return new Promise((resolve, reject) => {
    const items = {};
    glob(`${TARGET_PATH}**/package.json`, (er, files) => {
      files = files.filter((f) => !f.includes('node_modules'));
      async.each(files, (f, callback) => {
        fs.readFile(f, 'utf8', (err, data) => {
          if (err) throw err;
          const {name, version} = JSON.parse(data);
          if (name && version) {
            items[name] = version;
          }
          callback();
        });
      }, err => {
        if (err) reject();
        resolve(items);
      });
    });
  });
}

// function findUnresolvedImportErrors() {
//   return new Promise((resolve, reject) => {
//     let packageMap = {};
//     async.each(errorsFile, (f, callback) => {
//       if (!f.errorCount) {
//         return callback();
//       }
//       const modules = f
//         .messages
//         .filter((m) => m.ruleId === 'import/no-unresolved')
//         .map((m) => m.message.replace(/[^\']+\'([^\']+)\'\./, '$1'));
//       const pkg = findPackageParent(path.join(path.dirname(f.filePath), 'package.json'));
//       if (!packageMap[pkg]) {
//         packageMap[pkg] = new Set(modules);
//       } else {
//         modules.forEach((m) => {
//           packageMap[pkg].add(m);
//         });
//       }
//       return callback();
//     }, err => {
//       if (err) throw err;
//       resolve(packageMap);
//     });
//   });
// }

function findExtraneousImportErrors() {
  return new Promise((resolve, reject) => {
    let packageMap = {};
    async.each(errorsFile, (f, callback) => {
      if (!f.errorCount) {
        return callback();
      }
      const modules = f
        .messages
        .filter((m) => m.ruleId === 'import/no-extraneous-dependencies')
        .filter((m) => !m.message.endsWith('dependencies, not devDependencies.'))
        .map((m) => m.message.replace(/.+\'npm\si\s\-S\s([^']+)\'.+/, '$1'));
      console.log(modules);
      const pkg = findPackageParent(path.join(path.dirname(f.filePath), 'package.json'));
      const isTest = path.basename(f.filePath).endsWith('.test.js');
      if (!packageMap[pkg]) {
        packageMap[pkg] = {
          dependencies: !isTest ? new Set(modules) : new Set(),
          devDependencies: isTest ? new Set(modules) : new Set(),
        };
      } else {
        const obj = isTest ? 'devDependencies' : 'dependencies';
        modules.forEach((m) => {
          packageMap[pkg][obj].add(m);
        });
      }
      return callback();
    }, err => {
      if (err) throw err;
      resolve(packageMap);
    });
  });
}

async function fixImportError(versions) {
  const getVersion = (pkg) => versions[0][pkg] || versions[1][pkg];
  const packages = await findExtraneousImportErrors();
  async.each(Object.keys(packages), (jsonPath, callback) => {
    fs.readFile(jsonPath, 'utf8', (err, data) => { // open package file
      if (err) throw err;
      data = JSON.parse(data);
      const packageDependencies = packages[jsonPath];

      if (packageDependencies.dependencies.size > 0) { // set dependencies
        if (!data.dependencies) {
          data.dependencies = {};
        }
        packageDependencies.dependencies.forEach((module) => {
          if (data.devDependencies && data.devDependencies[module]) {
            // console.info(`Module '${module}' exists in devDependencies of ${data.name}, no need to insert.`);
            return;
          }
          const version = getVersion(module);
          if (!version) {
            console.warn(`Missing version for package '${module}'!`);
            return;
          }
          data.dependencies[module] = `${moduleIsClearScored(module) ? '^' : ''}${version}`;
        });
      }

      if (packageDependencies.devDependencies.size > 0) {
        if (!data.devDependencies) {
          data.devDependencies = {};
        }
        packageDependencies.devDependencies.forEach((module) => {
          if (data.dependencies && data.dependencies[module]) {
            // console.info(`Module '${module}' exists in dependencies of ${data.name}, no need to insert.`);
            return;
          }
          const version = getVersion(module);
          if (!version) {
            console.warn(`Missing version for package '${module}'!`);
            return;
          }
          data.devDependencies[module] = `${moduleIsClearScored(module) ? '^' : ''}${version}`;
        });
      }

      const indents = (jsonPath === MAIN_PKG) ? 4 : 2;
      const saveData = JSON.stringify(data, null, indents);
      fs.writeFile(jsonPath, saveData, err => { // close file
        if (err) throw err;
        callback();
      });
    });
  }, err => {
    if (err) throw err;
    return Promise.resolve();
  });
}

function findPackageParent(filePath) {
  if (fs.existsSync(filePath)) {
    return filePath;
  } else {
    return findPackageParent(path.join(filePath, '../..', 'package.json'));
  }
}

Promise
  .all([getPackageVersions(), getYarnVersion()])
  .then((versions) => fixImportError(versions))
  .then(() => console.log('Done'))
  .catch((err) => {
    throw err;
  });
